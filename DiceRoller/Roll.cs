﻿using DiceRoller.Dice.Generic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiceRoller
{
	class Roll
	{
        #region Fields

		#endregion

		#region Properties

		#endregion

		#region Constructors
		public Roll()
		{

		}
		#endregion

		#region Methods
        /// <summary>
        /// Rolls generic dice, 1d6 if no params given.
        /// </summary>
        /// <param name="sides"></param>
        /// <param name="noDice"></param>
		static public void RollGenericDice(int sides = 6, int noDice = 1)
		{
            Random rnd = new Random();
			for (int i = 0; i <= noDice; i++)
			{
                GenericDie die = new GenericDie(rnd,sides);                
			}
		}
		#endregion
	}
}
