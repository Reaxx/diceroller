﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DiceRoller
{
	public abstract class Die <T>
	{
        #region properties
        public List<T> Sides { get; protected set; }
        public int Count => Sides.Count;
        private int RolledNr { get; set; }
        public T RolledSide => Sides[RolledNr];
        protected Random Rnd { get; set; }
        #endregion

        #region constructor
        public Die(Random rnd)
		{
        
        }
        #endregion

        #region abstract methods
        /// <summary>
        /// Method for defining the sides of the dice
        /// </summary>
        protected abstract void DefineSides();
        #endregion

        public void Roll()
		{
            RolledNr = Rnd.Next(1, this.Count + 1);
		}
		public override string ToString()
		{
            return Sides.ToString();
		}
	}
}