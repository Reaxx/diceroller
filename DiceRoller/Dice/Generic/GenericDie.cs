﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiceRoller.Dice.Generic
{
    public class GenericDie : Die<int>
    {
        private readonly int NoSides;

        public GenericDie(Random rnd, int noSides) : base(rnd)
        {
            if(noSides < 1)
            {
                throw new IndexOutOfRangeException();
            }


            Rnd = rnd;
            Sides = new List<int>();
            NoSides = noSides;

            DefineSides();
        }

        protected override void DefineSides()
        {
            for (int i = 1; i <= NoSides; i++)
            {
                Sides.Add(i);
            }
        }
    }
}
