﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiceRoller.StarWars.Dices
{
    class AbilityDie : Die<Side>
    {
        public AbilityDie(Random rnd) : base(rnd)
        {

        }

        protected override void DefineSides()
        {
            Sides.Add(new Side( StarWarsSymbols.Blank ));
            Sides.Add(new Side( StarWarsSymbols.Sucess ));
            Sides.Add(new Side( StarWarsSymbols.Sucess ));
            Sides.Add(new Side( StarWarsSymbols.Sucess, StarWarsSymbols.Sucess));
            Sides.Add(new Side( StarWarsSymbols.Advantage ));
            Sides.Add(new Side( StarWarsSymbols.Advantage ));
            Sides.Add(new Side( StarWarsSymbols.Sucess, StarWarsSymbols.Advantage ));
            Sides.Add(new Side( StarWarsSymbols.Advantage, StarWarsSymbols.Advantage ));
        }
    }
}
