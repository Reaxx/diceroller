﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using DiceRoller.Dice.Generic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiceRoller.Dice.Generic.Tests
{
    [TestClass()]
    public class GenericDieTests
    {
        [TestMethod()]
        //Tests with valid asumption
        public void GenericDieTest()
        {
            Random rnd = new Random();
            int max = 10;
            int rndNr = rnd.Next(1,max);
            GenericDie die = new GenericDie(rnd, max);

            Assert.IsTrue(die.Sides[rndNr] == rndNr+1);
        }

        //Testing negative values (should throw exception)
        [TestMethod()]
        [ExpectedException(typeof(IndexOutOfRangeException))]
        public void GenericDieTest2()
        {
            Random rnd = new Random();
            GenericDie die = new GenericDie(rnd, -1);        
        }
    }
}